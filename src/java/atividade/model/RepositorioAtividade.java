/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atividade.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author emerson
 */
public class RepositorioAtividade {
    
     private static List<Atividade> atividades = new ArrayList<Atividade>();

    public static void salvar(Atividade e) {
        Atividade eq = getAtividade(e.getCodigo());
        if (eq == null) {
            atividades.add(e);
        }
    }

    public static Atividade getAtividade(Integer codigo) {
        for (Atividade e : atividades) {
            if (e.getCodigo().compareTo(codigo) == 0) {
                return e;
            }
        }
        return null;
    }

    public static List<Atividade> getAtividades() {
        return atividades;
    }
    
}
