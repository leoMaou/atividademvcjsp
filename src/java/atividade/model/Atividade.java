package atividade.model;

import java.util.Date;
import java.util.List;

/**
 *
 * @author emerson
 */
public class Atividade {
    
    
    private Integer codigo;
    private String descricao;
    private Date dataCadastro;
    private Date dataConclusao;
    private Integer estagio;
    private List<Nota> notas;

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataCadastro() {
        return dataCadastro;
    }

    public void setDataCadastro(Date dataCadastro) {
        this.dataCadastro = dataCadastro;
    }

    public Date getDataConclusao() {
        return dataConclusao;
    }

    public void setDataConclusao(Date dataConclusao) {
        this.dataConclusao = dataConclusao;
    }

    public Integer getEstagio() {
        return estagio;
    }

    public void setEstagio(Integer estagio) {
        this.estagio = estagio;
    }

    public List<Nota> getNotas() {
        return notas;
    }

    public void setNotas(List<Nota> notas) {
        this.notas = notas;
    }
    
     public Nota getNota(Integer codigo) {
        for (Nota n : notas){
            if (n.getCodigo().compareTo(codigo) == 0){
                return n;
            }
        }
        return null;
    }

    public void addNota(Nota nota) {
        Nota n = getNota(nota.getCodigo());
        if (n!= null){
            n.setNota(nota.getNota());
        }else{
            this.notas.add(nota);
        }
    }
    
    
}
