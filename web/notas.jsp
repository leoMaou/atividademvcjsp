<%-- 
    Document   : notas
    Created on : 10/10/2018, 08:00:43
    Author     : emerson
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
         <h1>Cadastro de notas</h1>
        Atividade: (${e.codigo}) - ${e.descricao}
        <br/>
        <br/>
        Notas:
        <hr>
        <table border="1">
            <tr>
                <th id="codigo" >Código</th>
                <th id="descricao">Nota</th>
                <th id="dataCadastro">Data de cadastro</th>
                <th>Ações</th>
            <tr>    
            <c:forEach var="f" items="${e.notas}" >
                <tr>
                    <td>${f.codigo}</td>
                    <td>${f.nota}</td>
                    <td>${f.data}</td>
                    <td>
                        <a href="editarNota?codigoAtv=${e.codigo}&codigo=${f.codigo}">Editar</a> - 
                        <a href="excluirNota?codigoAtv=${e.codigo}&codigo=${f.codigo}">Excluir</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <br/>
        Nova nota:
        <hr>
        <form method="POST" action="salvarNota">
            Código: <input type="text" name="codigo" value="${n.codigo}" ><br>
            Nota: <input type="text" name="nota" value="${n.nota}" ><br>
            <input style="display: none" type="text" name="codigoAtv" value="${e.codigo}" ><br>
            <button>Salvar</button>
        </form>
        <br/>
        <br/>
        <a href="listar">Voltar</a> <br>
    </body>
</html>
