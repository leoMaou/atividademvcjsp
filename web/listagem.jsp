<%-- 
    Document   : listagem
    Created on : 30/09/2018, 12:22:50
    Author     : emerson
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <a href="novo">Nova atividade</a> <br>
        <hr>
        <table border="1">
            <tr>
                <th id="codigo" >Código</th>
                <th id="descricao">Descrição</th>
                <th id="estagio">Estágio</th>
                <th id="dataCadastro">Data de cadastro</th>
                <th>Ações</th>
            <tr>    
            <c:forEach var="e" items="${lista}" >
                <tr>
                    <td>${e.codigo}</td>
                    <td>${e.descricao}</td>
                    <td>${e.estagio}%</td>
                    <td>${e.dataCadastro}</td>
                    <td>
                        <a href="editar?codigo=${e.codigo}">Editar</a> - 
                        <a href="remover?codigo=${e.codigo}">Excluir</a> - 
                        <a href="concluir?codigo=${e.codigo}">Concluir</a> - 
                        <a href="notas?codigo=${e.codigo}">Notas</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
