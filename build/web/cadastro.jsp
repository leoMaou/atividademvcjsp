<%-- 
    Document   : cadastro
    Created on : 10/10/2018, 07:58:18
    Author     : emerson
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Cadastro de atividades</h1>
        <form method="POST" action="salvar">
            Código: <input type="text" name="codigo" value="${e.codigo}" ><br>
            Descrição: <input type="text" name="descricao" value="${e.descricao}" ><br>
            Estágio: <input type="text" name="estagio" value="${e.estagio}" ><br>
            <button>Salvar</button>
        </form>
        <br/>
        <br/>
        <hr>
        <table border="1">
            <tr>
                <th id="codigo" >Código</th>
                <th id="descricao">Descrição</th>
                <th id="estagio">Estágio</th>
                <th id="dataCadastro">Data de cadastro</th>
                <th>Ações</th>
            <tr>    
            <c:forEach var="f" items="${lista}" >
                <tr>
                    <td>${f.codigo}</td>
                    <td>${f.descricao}</td>
                    <td>${f.estagio}%</td>
                    <td>${f.dataCadastro}</td>
                    <td>
                        <a href="editar?codigo=${f.codigo}">Editar</a> - 
                        <a href="remover?codigo=${f.codigo}">Excluir</a>
                    </td>
                </tr>
            </c:forEach>
        </table>
        <br/>
        <br/>
        <a href="listar">Voltar</a> <br>
    </body>
    </body>
</html>
